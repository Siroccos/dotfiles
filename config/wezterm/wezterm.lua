local wezterm = require("wezterm")

local config = {}

if wezterm.config_builder then
	config = wezterm.config_builder()
end

config.enable_wayland = false

-- Theme
config.color_scheme = 'Rosé Pine Dawn (Gogh)'

-- Font
config.font = wezterm.font({
  -- family = 'FiraCode Nerd Font',
  -- family = 'MonaspiceNe Nerd Font',
  -- family = 'RecMonoLinear Nerd Font',
  family = 'IosevkaCactus Nerd Font',
  -- weight = 'Medium',
})

-- config.font_rules = {
--   {
--     italic = true,
--     font = wezterm.font {
--       family = 'RecMonoCasual Nerd Font',
--       -- family = 'MonaspiceAr Nerd Font',
--       style = 'Italic',
--     },
--   },
-- }

config.font_size = 12.0
config.harfbuzz_features = {
	'calt', 'clig', 'liga', 'dlig',
	-- 'ss01', 'ss02', 'ss03', 'ss04', 'ss05', 'ss06', 'ss07'
}

config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0
}

config.check_for_updates = false

config.force_reverse_video_cursor = false

config.cursor_thickness = 0.5

config.line_height = 1.0

-- Tab bar masqué s'il y a qu'un seul onglet
config.hide_tab_bar_if_only_one_tab = true

-- config.freetype_load_flags = "NO_HINTING"
-- config.freetype_load_flags = "FORCE_AUTOHINT"

-- Performance
config.animation_fps = 60
config.cursor_blink_ease_in = "EaseIn"
config.cursor_blink_ease_out = "EaseOut"

config.default_cursor_style = 'BlinkingBlock'

config.underline_position = -2

-- Scroll to prompt
local act = wezterm.action

config.keys = {
  -- Scroll prompt
  { key = 'UpArrow', mods = 'SHIFT', action = act.ScrollToPrompt(-1) },
  { key = 'DownArrow', mods = 'SHIFT', action = act.ScrollToPrompt(1) },

  -- Change pane
  { key = 'h', mods = 'CTRL|SHIFT', action = act.ActivatePaneDirection 'Left' },
  { key = 'l', mods = 'CTRL|SHIFT', action = act.ActivatePaneDirection 'Right' },
  { key = 'j', mods = 'CTRL|SHIFT', action = act.ActivatePaneDirection 'Down' },
  { key = 'k', mods = 'CTRL|SHIFT', action = act.ActivatePaneDirection 'Up' },

  -- Show debug overlay
  { key = 'o', mods = 'CTRL|SHIFT', action = act.ShowDebugOverlay },

  -- Split current pane
  -- { key = 'v', mods = 'CTRL|SHIFT', action = act.SplitVertical{domain='CurrentPaneDomain'} },
  { key = 's', mods = 'CTRL|SHIFT', action = act.SplitHorizontal{domain='CurrentPaneDomain'} },

  -- Switch color mode
  { key = 'm', mods = 'CTRL|SHIFT', action = wezterm.action_callback(function(win, pane)
  		if wezterm.GLOBAL.theme == 'dark' then
  			wezterm.GLOBAL.theme = 'light'
  		else
  			wezterm.GLOBAL.theme = 'dark'
  		end
	  	wezterm.background_child_process {
	  		'/home/cdo/switch_theme.py', wezterm.GLOBAL.theme
	  	}
  	end),
	}
}

config.mouse_bindings = {
	{
		event = { Down = { streak = 3, button = 'Left' } },
		action = act.SelectTextAtMouseCursor 'SemanticZone',
		mods = 'NONE',
	},
}

-- Disactivation du beep
config.audible_bell = "Disabled"

return config
